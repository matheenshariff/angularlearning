import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items: any;
  grandTotal: any;


  constructor() {

    this.items = [
      { id: 1, item_name: 'Rice', price: 1000, count: 0, total: 0 },
      { id: 2, item_name: 'Wheat', price: 180, count: 0, total: 0 },
      { id: 3, item_name: 'Cashew', price: 350, count: 0, total: 0 },
      { id: 4, item_name: 'Butter', price: 90, count: 0, total: 0 },
      { id: 5, item_name: 'Oil', price: 110, count: 0, total: 0 },
    ];

    this.grandTotal = 0;

  }

  ngOnInit(): void {

  }

  incrementCount(event, item) {
    let index = this.items.indexOf(item);
    item.count = item.count + 1;
    item.total = item.count * item.price;
    this.items[index] = item;
    console.log('item', item);
    this.calculateSumTotal();
  }

  decrementCount(event, item) {
    let index = this.items.indexOf(item);
    if (item.count != 0) {
      item.count = item.count - 1;
      item.total = item.count * item.price;
      this.items[index] = item;
      this.calculateSubTotal();
    }

  }

  calculateSumTotal() {
    let sum = 0;
    for (var i = 0; i < this.items.length; i++) {
      sum = sum + this.items[i].total
    }
    this.grandTotal = sum;
  }
  
  calculateSubTotal() {
    let sum = 0;
    for (var i = 0; i < this.items.length; i++) {
      sum = this.items[i].total - sum;
    }
    this.grandTotal = sum;

  }



}
